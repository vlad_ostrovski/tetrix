﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BoardView
{
    private readonly GameObject board;

    private List<Plane> planes;

    public BoardView(GameObject board) {
        this.board = board;
    }

    public void initBoardView() {
        planes = new List<Plane>();

        GameObject.Find("Bounds").transform.localScale = new Vector3(GameConfig.Instance.BoardWidth / 10f, GameConfig.Instance.BoardHeight / 10f, GameConfig.Instance.BoardDepth / 10f);
        Vector4 wallTiling = new Vector4(GameConfig.Instance.BoardHeight, GameConfig.Instance.BoardWidth, 0, 0);
        GameObject.Find("WallX").GetComponent<MeshRenderer>().material.SetVector("_Tiling", wallTiling);
        GameObject.Find("WallMinusX").GetComponent<MeshRenderer>().material.SetVector("_Tiling", wallTiling);
        GameObject.Find("WallZ").GetComponent<MeshRenderer>().material.SetVector("_Tiling", wallTiling);
        GameObject.Find("WallMinusZ").GetComponent<MeshRenderer>().material.SetVector("_Tiling", wallTiling);

        Vector4 floorTiling = new Vector4(GameConfig.Instance.BoardWidth, GameConfig.Instance.BoardDepth, 0, 0);
        GameObject.Find("Floor").GetComponent<MeshRenderer>().material.SetVector("_Tiling", floorTiling);
    }

    /// <summary>
    /// Adds a shape to the board view at the shape's current position.
    /// </summary>
    public void addShape(ShapeController shapeController) {
        Vector3 shapePos = shapeController.GetShapePosition();

        List<Vector3> blockPositions = shapeController.getLocalShapeBlockPositions();
        for (int i = 0; i < blockPositions.Count; i++) {
            GameObject block = ShapeBlockUtils.getBlock(board.transform);
            Vector3 pos = blockPositions[i];

            block.transform.SetParent(board.transform);
            ShapeBlockController shapeBlockController = block.GetComponent<ShapeBlockController>();
            shapeBlockController.placeAt(new Vector3(Mathf.RoundToInt(pos.x + shapePos.x), Mathf.RoundToInt(pos.y + shapePos.y), Mathf.RoundToInt(pos.z + shapePos.z)));
            shapeBlockController.setColor(shapeController.getShapeColor());

            addToPlane(block, Mathf.RoundToInt(pos.y + shapePos.y));
        }
    }

    private void addToPlane(GameObject block, int y) {
        if (y >= planes.Count) {
            for (int i = 0; i <= y - planes.Count; i++) {
                planes.Add(new Plane());
            }
        }

        planes[y].Blocks.Add(block);

        block.GetComponent<ShapeBlockController>().bringToPlane(y);
     }

    /// <summary>
    /// Starts an explode animation on all blocks on the board view
    /// </summary>
    public void exoloadAll() {
        planes.ForEach(plane => {
            plane.Blocks.ForEach(o => o.GetComponent<ShapeBlockController>().fadeOut());
        });
    }

    /// <summary>
    /// Removes given planes from the board view (given by y indices)
    /// </summary>
    public void removePlanes(List<int> fullPlanes) {
        if (fullPlanes.Count == 0) {
            return;
        }

        fullPlanes.ForEach(idx => {
            planes[idx].Blocks.ForEach(block => block.GetComponent<ShapeBlockController>().fadeOut());
            planes[idx].Remove = true;
        });

        planes.RemoveAll(plane => plane.Remove);

        int planeCnt = planes.Count;
        for (int i = 0; i < planeCnt; i++) {
            planes[i].Blocks.ForEach(block => block.GetComponent<ShapeBlockController>().bringToPlane(i));
        }

    }

    private class Plane {
        public Plane() {
            Blocks = new List<GameObject>();
            Remove = false;
        }

        public List<GameObject> Blocks;
        public bool Remove;
    }
}
