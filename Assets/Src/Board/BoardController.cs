﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BoardController : MonoBehaviour
{
    private BoardModel boardModel;
    private BoardView boardView;

    public void Start() {
        boardModel = new BoardModel(GameConfig.Instance.BoardWidth, GameConfig.Instance.BoardHeight, GameConfig.Instance.BoardDepth);
        boardView = new BoardView(this.gameObject);
        boardView.initBoardView();
    }

    /// <summary>
    /// Returns The Board size.
    /// </summary>
    public Vector3Int GetBoardSize() {
        return new Vector3Int(boardModel.Width, boardModel.Height, boardModel.Depth);
    }

    /// <summary>
    /// Checks for collision of a shape with the board along a certain axis. <br/>
    /// Note: The axis should be the <b> opposite </b> of the shape's movement direction. Use Vector3.zero to check all axis.
    /// </summary>
    public bool doesCollide(ShapeController shapeController, Vector3 axis) {
        return boardModel.doesCollide(shapeController, axis);
    }

    /// <summary>
    /// Adds a shape to the board at the shape's current position.
    /// </summary>
    public void addShape(ShapeController shapeController) {
        boardModel.addShape(shapeController);
        boardView.addShape(shapeController);
    }

    /// <summary>
    /// Clears full planes from the board and returns the number of cleared planes
    /// </summary>
    public int clearFullPlanes() {
        List<int> fullPlanes = boardModel.removeFullPlanes();
        boardView.removePlanes(fullPlanes);

        return fullPlanes.Count;
    }

    /// <summary>
    /// Removes all blocks from the board
    /// </summary>
    public void removeAll() {
        boardView.exoloadAll();
    }
}
