﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class BoardModel {

    public int Width {get; private set;}
    public int Height { get; private set; }
    public int Depth { get; private set; }

    private readonly List<Plane> planes;

    public BoardModel(int w, int h, int d) {
        Width = w;
        Height = h;
        Depth = d;

        planes = new List<Plane>();
    }

    /// <summary>
    /// Adds a shape to the board at the shape's current position.
    /// </summary>
    public void addShape(ShapeController shapeController) {
        Vector3 shapePos = shapeController.GetShapePosition();

        List<Vector3> blocks = shapeController.getLocalShapeBlockPositions();
        blocks.ForEach(block => {
            addBlock(Mathf.RoundToInt(block.x + shapePos.x), Mathf.RoundToInt(block.y + shapePos.y), Mathf.RoundToInt(block.z + shapePos.z));
        });
    }

    private void addBlock(int x, int y, int z) {
        int numPlanes = planes.Count;
        for (int i = 0; i < y - numPlanes + 1; i++) {
            Plane p = new Plane(Width, Depth);
            planes.Add(p);
        }

        planes[y].Blocks.Set(getBitIdx(x, z), true);
        planes[y].Counter++;
    }

    private int getBitIdx(int x, int z) {
        int res = z * Width + x;
        return res;
    }

    /// <summary>
    /// Checks for collision of a shape with the board along a certain axis. <br/>
    /// Note: The axis should be the <b> opposite </b> of the shape's movement direction. Use Vector3.zero to check all axis.
    /// </summary>
    public bool doesCollide(ShapeController shapeController, Vector3 axis) {
        Vector3 pos = shapeController.GetShapePosition();
        List<Vector3> localTestPoints;
        if (axis.Equals(Vector3.zero)) {
            localTestPoints = shapeController.getLocalShapeBlockPositions();
        } else {
            localTestPoints = shapeController.getLocalContactPoints(axis);
        }
        
        List<Vector3> testPoints = new List<Vector3>();

        foreach (Vector3 p in localTestPoints) {
            testPoints.Add(new Vector3(p.x + pos.x, Mathf.FloorToInt(p.y + pos.y), p.z + pos.z));
            if (!axis.Equals(Vector3.up)) {
                testPoints.Add(new Vector3(p.x + pos.x, Mathf.CeilToInt(p.y + pos.y), p.z + pos.z));
            }
        }
        
        foreach (Vector3 p in testPoints) {
            if (isPositionBusy(p)) {
                 return true;
            }
        }


        return false;
    }

    private bool isPositionBusy(Vector3 pos) {
        if (pos.y == -1) {
            return true;
        }

        if (pos.y >= planes.Count) {
            return false;
        }

        return planes[Mathf.FloorToInt(pos.y)].Blocks.Get(getBitIdx(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.z)));
    }

    /// <summary>
    /// Removes full planes from the board and returns the y coordinate of removed planes
    /// </summary>
    public List<int> removeFullPlanes() {
        var res = new List<int>();
        for (int i = 0; i < planes.Count; i++) {
            if (planes[i].Counter == Width * Depth) {
                planes[i].Remove = true;
                res.Add(i);
            }
        }

        planes.RemoveAll((p) => p.Remove == true);

        return res;
    }

    private class Plane {
        public Plane(int width, int depth) {
            Blocks = new BitArray(width * depth); ;
            Remove = false;
            Counter = 0;
        }

        public BitArray Blocks;
        public bool Remove;
        public int Counter;
    }
}
