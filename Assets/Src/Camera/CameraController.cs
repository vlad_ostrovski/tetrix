﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject DirectionMarker;

    private float thetha;
    private float angle;
    private float radius;
    private Vector3 center;


    void Start()
    {
        thetha = Mathf.PI / 2.4f;
        angle = 3*Mathf.PI/2f;

        setCenterAndRadius(thetha);
        setCameraAtAngle(angle);

        DirectionMarker.transform.position = new Vector3(GameConfig.Instance.BoardWidth / 2f, 0, -0.5f);
        DirectionMarker.transform.rotation = Quaternion.Euler(90, 180, 0);
    }

    void Update()
    {
        if (Input.GetMouseButton(0)) {
            float axis = Input.GetAxis("Horizontal");
            angle -= axis*0.01f;

            if (angle < 0) {
                angle += 2 * Mathf.PI;
            } else if (angle > 2 * Mathf.PI) {
                angle -= 2 * Mathf.PI;
            }

            float axisY = Input.GetAxis("Vertical");
            thetha += axisY * 0.01f;
            thetha = Mathf.Clamp(thetha, -Mathf.PI / 4f, Mathf.PI / 2f - 0.001f);

            setCenterAndRadius(thetha);
            setCameraAtAngle(angle);

            float deg = angle * (180 / Mathf.PI);

            if (deg >= 225 && deg < 315) {
                DirectionMarker.transform.position = new Vector3(GameConfig.Instance.BoardWidth / 2f, 0, -0.5f);
                DirectionMarker.transform.rotation = Quaternion.Euler(90, 180, 0);
            } else if (deg >= 315 && deg < 360 || deg >= 0 && deg < 45) {
                DirectionMarker.transform.position = new Vector3(GameConfig.Instance.BoardWidth + 0.5f, 0, GameConfig.Instance.BoardDepth / 2f);
                DirectionMarker.transform.rotation = Quaternion.Euler(90, 90, 0);
            } else if (deg >= 45 && deg < 135) {
                DirectionMarker.transform.position = new Vector3(GameConfig.Instance.BoardWidth / 2f, 0, GameConfig.Instance.BoardDepth + 0.5f);
                DirectionMarker.transform.rotation = Quaternion.Euler(90, 0, 0);
            } else {
                DirectionMarker.transform.position = new Vector3(-0.5f, 0, GameConfig.Instance.BoardDepth / 2f);
                DirectionMarker.transform.rotation = Quaternion.Euler(90, -90, 0);
            }
        }



    }
    private void setCenterAndRadius(float thetha) {
        center = new Vector3(GameConfig.Instance.BoardWidth / 2f, GameConfig.Instance.BoardHeight + 7 * Mathf.Sin(thetha), GameConfig.Instance.BoardDepth / 2f);
        radius = (Mathf.Max(GameConfig.Instance.BoardWidth / 2f + 2, GameConfig.Instance.BoardDepth / 2f + 2) + 12) * Mathf.Cos(thetha);
    }

    private void setCameraAtAngle(float a) {
        this.transform.position = new Vector3(center.x + radius * Mathf.Cos(angle), center.y, center.z + radius * Mathf.Sin(angle));
        this.transform.LookAt(new Vector3(center.x + radius * Mathf.Cos(Mathf.PI + angle), 0, center.z + radius * Mathf.Sin(Mathf.PI + angle)));
    }

    /// <summary>
    /// Returns the current horizontal angle of the camera
    /// </summary>
    public float getCurrentAngle() {
        return angle;
    }
}
