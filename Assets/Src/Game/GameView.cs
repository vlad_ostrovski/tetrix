﻿using UnityEngine;
using UnityEngine.UI;

public class GameView {
    private readonly GameObject game;

    private readonly GameObject scoreText;
    private readonly GameObject nextShapeUI;
    private readonly GameObject lostScreen;
    private readonly GameObject preGameScreen;
    private readonly GameObject finalScoreText;

    public GameView(GameObject game) {
        this.game = game;

        scoreText = GameObject.Find("Game/UI/Canvas/ScoreText");
        nextShapeUI = GameObject.Find("Game/UI/Canvas/NextShapeUI");
        lostScreen = GameObject.Find("Game/UI/Canvas/LostScreen");
        preGameScreen = GameObject.Find("Game/UI/Canvas/PreGameScreen");
        finalScoreText = GameObject.Find("Game/UI/Canvas/LostScreen/FinalScore");
    }

    public void showLostScreen(int score) {
        scoreText.SetActive(false);
        nextShapeUI.SetActive(false);

        finalScoreText.GetComponent<Text>().text = score.ToString();
        lostScreen.SetActive(true);
    }

    public void updateScoreText(int score) {
        scoreText.GetComponent<Text>().text = "Score: " + score;
    }

    public void showGameScreen() {
        scoreText.SetActive(true);
        nextShapeUI.SetActive(true);
        preGameScreen.SetActive(false);
    }
}
