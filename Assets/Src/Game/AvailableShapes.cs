﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using UnityEngine;

public class AvailableShapes
{
    public static AvailableShapes Instance = new AvailableShapes();

    private readonly Dictionary<ShapeModel, int> availableShapes;
    private int weightSum;
    private int[] weightIntegral;

    private AvailableShapes() {
        availableShapes = new Dictionary<ShapeModel, int>();
    }

    /// <summary>
    /// Load available shapes from configuration file. See configuration file for the format <br/>
    /// Note: a wrongly formated or missing configuration file will fall back to a default one block shape
    /// </summary>
    public void loadAvailableShapes() {
        string configFile = Path.Combine(Application.dataPath, "Config/ShapeConfig.xml");

        try {
            var serializer = new XmlSerializer(typeof(ShapeConfigListDTO));
            using (var stream = new FileStream(configFile, FileMode.Open)) {
                ShapeConfigListDTO config = serializer.Deserialize(stream) as ShapeConfigListDTO;
                if (config.Shapes.Count == 0) {
                    throw new Exception("Shape config must contain at least one shape");
                }

                weightSum = 0;
                config.Shapes.ForEach(shapeDTO => {
                    Color color;
                    ColorUtility.TryParseHtmlString(shapeDTO.color, out color);
                    availableShapes.Add(ShapeModel.initFromString(shapeDTO.Width, shapeDTO.Height, shapeDTO.Depth, color, shapeDTO.Description, shapeDTO.PivotIdx), shapeDTO.Weight);
                    weightSum += shapeDTO.Weight;
                });

                weightIntegral = new int[availableShapes.Count];

                for (int i = 0; i < availableShapes.Count; i++) {
                    weightIntegral[i] = availableShapes.Values.ElementAt(i) + (i == 0 ? 0 : weightIntegral[i - 1]);
                }
            }
        } catch (Exception e) {
            Console.Error.WriteLine(e);
            Console.Error.WriteLine("Couldn't read shapes config file or file is not formatted properly. Using default values.");
            setDefaultValues();
        }

    }

    private void setDefaultValues() {
        availableShapes.Add(ShapeModel.initFromString(1, 1, 1, Color.white, "1", 1), 1);
        weightSum = 1;
        weightIntegral = new int[1];
        weightIntegral[0] = 1;
    }

    /// <summary>
    /// Returns a random shape model from the available shapes, with the chance defined by the given weights
    /// </summary>
    public ShapeModel getRandomShape() {
        int idx = UnityEngine.Random.Range(0, weightSum);

        for (int i = 0; i < weightIntegral.Length; i++) {
            if (idx < weightIntegral[i]) {
                return availableShapes.Keys.ElementAt(i);
            }
        }

        return ShapeModel.initFromShapeModel(availableShapes.ElementAt(idx).Key);
    }


    [XmlRoot("ShapeConfig")]
    public class ShapeConfigListDTO
    {
        [XmlArray("Shapes")]
        [XmlArrayItem("Shape")]
        public List<ShapeConfigDTO> Shapes = new List<ShapeConfigDTO>();
    }

    [XmlRoot("Shape")]
    public class ShapeConfigDTO
    {
        [XmlAttribute("width")]
        public int Width { get; set; }
        [XmlAttribute("height")]
        public int Height { get; set; }
        [XmlAttribute("depth")]
        public int Depth { get; set; }
        [XmlAttribute("color")]
        public string color { get; set; }
        [XmlAttribute("description")]
        public string Description { get; set; }
        [XmlAttribute("pivotIdx")]
        public int PivotIdx { get; set; }
        [XmlAttribute("weight")]
        public int Weight { get; set; }
    }
}
