﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public class GameConfig
{
    public static GameConfig Instance = new GameConfig();
    
    public int BoardWidth { get; private set; }

    public int BoardHeight { get; private set; }

    public int BoardDepth { get; private set; }

    public float InitialGameSpeed { get; private set; }


    public float GameAcceleration { get; private set; }

    private GameConfig() {
    }

    /// <summary>
    /// Loads the game configuration from the configuration file. See configuration file for formatting.
    /// </summary>
    public void loadGameConfig() {
        string configFile = Path.Combine(Application.dataPath, "Config/GameConfig.xml");

        try {
            var serializer = new XmlSerializer(typeof(GameConfigDTO));
            using (var stream = new FileStream(configFile, FileMode.Open)) {
                GameConfigDTO config = serializer.Deserialize(stream) as GameConfigDTO;
                BoardWidth = config.Width;
                BoardHeight = config.Height;
                BoardDepth = config.Depth;
                InitialGameSpeed = config.InitialSpeed;
                GameAcceleration = config.Acceleration;
            }
        } catch (Exception e) {
            Console.Error.WriteLine(e);
            Console.Error.WriteLine("Couldn't read game config file or file is not formatted properly. Using default values.");
            setDefaultValues();
        }
 
    }

    private void setDefaultValues() {
        // default values
        BoardWidth = 7;
        BoardHeight = 10;
        BoardDepth = 7;
        InitialGameSpeed = 1;
        GameAcceleration = 0.001f;
    }

    [XmlRoot("GameConfig")]
    public class GameConfigDTO
    {
        [XmlAttribute("width")]
        public int Width { get; set; }
        [XmlAttribute("height")]
        public int Height { get; set; }
        [XmlAttribute("depth")]
        public int Depth { get; set; }
        [XmlAttribute("initialSpeed")]
        public float InitialSpeed { get; set; }
        [XmlAttribute("acceleration")]
        public float Acceleration { get; set; }
    }
}
