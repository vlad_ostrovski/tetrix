﻿using UnityEngine;
using UnityEngine.SceneManagement;

using static InputUtils;

public class GameController : MonoBehaviour
{
    public GameObject PlayableShape;
    public GameObject NextShape;
    public GameObject HintShape;

    public Camera MainCamera;
    public Camera PreviewCamera;

    private GameModel gameModel;
    private GameView gameView;

    private BoardController boardController;

    private ShapeController hintShapeController;
    private ShapeController playableShapeController;
    private ShapeController nextShapeController;

    private CameraController mainCameraController;

    void Awake() {
        GameConfig.Instance.loadGameConfig();
        AvailableShapes.Instance.loadAvailableShapes();
    }

    void Start()
    {
        gameView = new GameView(this.gameObject);
        gameModel = new GameModel();

        boardController = FindObjectOfType<BoardController>();

        nextShapeController = NextShape.GetComponent<ShapeController>();
        hintShapeController = HintShape.GetComponent<ShapeController>();
        playableShapeController = PlayableShape.GetComponent<ShapeController>();

        mainCameraController = MainCamera.GetComponent<CameraController>();

        gameModel.GameState = GameModel.State.PRE_GAME;
    }

    private void initPlayableShapePosition() {
        var size = playableShapeController.getShapeSize();
        Vector3 basePosition = new Vector3((int)(GameConfig.Instance.BoardWidth / 2 - size.x / 2), GameConfig.Instance.BoardHeight, (int)(GameConfig.Instance.BoardDepth / 2 - size.z / 2));
        playableShapeController.moveTo(basePosition);
    }

    private void initShapeHint() {
        hintShapeController.destroy();
        hintShapeController.initNextShape(playableShapeController);
        hintShapeController.setHintShape();
        updateShapeHint();
    }

    private void updateShapeHint() {
        hintShapeController.moveTo(playableShapeController.GetShapePosition());

        Vector3 initial = hintShapeController.GetShapePosition();
        for (int i = (int)initial.y; i >= -1; i--) {
            hintShapeController.moveTo(new Vector3(initial.x, i, initial.z));
            if (boardController.doesCollide(hintShapeController, Vector3.up)) {
                hintShapeController.moveTo(new Vector3(initial.x, i + 1, initial.z));
                break;
            }
        }
    }

    private void initNextShapePreview() {
        nextShapeController.destroy();

        nextShapeController.initNextShape();
        nextShapeController.moveTo(new Vector3(0, 0.75f * GameConfig.Instance.BoardHeight, 80));

        var size = nextShapeController.getShapeSize();
        PreviewCamera.transform.position = new Vector3(size.x / 2f, size.y / 2f + 0.75f*GameConfig.Instance.BoardHeight, 70);
        PreviewCamera.transform.rotation.SetLookRotation(new Vector3(size.x / 2f, size.y / 2f + 0.75f * GameConfig.Instance.BoardHeight, 80 + size.z / 2f), Vector3.up);
    }

    void Update() {
        if (gameModel.GameState == GameModel.State.PLAYING) {
            gameModel.update();
            playableShapeController.drop(gameModel.CurrentGameSpeed);
            nextShapeController.rotatePreview();

            Vector3 playableShapePos = playableShapeController.GetShapePosition();

            // Check Collision        
            if (boardController.doesCollide(playableShapeController, Vector3.up)) {
                playableShapeController.moveBy(new Vector3(0, Mathf.Ceil(playableShapePos.y) - playableShapePos.y, 0));
                boardController.addShape(playableShapeController);

                if (Mathf.Ceil(playableShapePos.y) + playableShapeController.getShapeSize().y > boardController.GetBoardSize().y) {
                    // Shape got out of the board "roof". Player lost.
                    playableShapeController.destroy();
                    hintShapeController.destroy();
                    nextShapeController.destroy();

                    boardController.removeAll();

                    gameView.showLostScreen(gameModel.Score);

                    gameModel.GameState = GameModel.State.LOST;
                    return;
                }

                int removed = boardController.clearFullPlanes();
                gameModel.Score += removed;

                gameView.updateScoreText(gameModel.Score);

                playableShapeController.destroy();
                playableShapeController.initNextShape(nextShapeController);

                initPlayableShapePosition();
                initNextShapePreview();
                initShapeHint();
            }

            Vector3Int boardSize = boardController.GetBoardSize();
            Vector3Int playableShapeSize = playableShapeController.getShapeSize();

            checkMovementInput(playableShapeController, playableShapePos, playableShapeSize, boardSize);
            checkRotationInput(playableShapeController, boardSize);
        } else if (gameModel.GameState == GameModel.State.LOST) {
            // restart scene (game)
            if (Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.KeypadEnter)) {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }

            if (Input.GetKeyUp(KeyCode.Escape) || Input.GetKeyUp(KeyCode.Q)) {
                Application.Quit();
            }
        } else if (gameModel.GameState == GameModel.State.PRE_GAME) {
            if (Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.KeypadEnter)) {
                playableShapeController.initNextShape();

                initPlayableShapePosition();
                initShapeHint();
                initNextShapePreview();

                gameView.showGameScreen();

                gameModel.GameState = GameModel.State.PLAYING;
            }

            if (Input.GetKeyUp(KeyCode.Escape) || Input.GetKeyUp(KeyCode.Q)) {
                Application.Quit();
            }

        }
    }

    private void checkRotationInput(ShapeController playableShapeController, Vector3Int boardSize) {
        float currentCameraAngle = mainCameraController.getCurrentAngle();

        if (Input.GetKeyDown(getCurrentKeyFor(MovementType.ROTATEX, currentCameraAngle))) {
            playableShapeController.rotateAroundX();
            Vector3 newPos = playableShapeController.GetShapePosition();
            Vector3 newSize = playableShapeController.getShapeSize();
            
            if (newPos.y < 0 || newPos.z < 0 || newPos.z + newSize.z > boardSize.z || boardController.doesCollide(playableShapeController, Vector3.zero)) {
                playableShapeController.rotateAroundX();
                playableShapeController.rotateAroundX();
                playableShapeController.rotateAroundX();
            } else {
                hintShapeController.rotateAroundX();
                updateShapeHint();
            }
        }

        if (Input.GetKeyDown(KeyCode.E)) {
            playableShapeController.rotateAroundY();

            Vector3 newPos = playableShapeController.GetShapePosition();
            Vector3 newSize = playableShapeController.getShapeSize();

            if (newPos.x < 0 || newPos.z < 0 || newPos.z + newSize.z > boardSize.z || newPos.x + newSize.x > boardSize.x || boardController.doesCollide(playableShapeController, Vector3.zero)) {
                playableShapeController.rotateAroundY();
                playableShapeController.rotateAroundY();
                playableShapeController.rotateAroundY();
            } else {
                hintShapeController.rotateAroundY();
                updateShapeHint();
            }
        }

        if (Input.GetKeyDown(getCurrentKeyFor(MovementType.ROTATEZ, currentCameraAngle))) {
            playableShapeController.rotateAroundZ();
            Vector3 newPos = playableShapeController.GetShapePosition();
            Vector3 newSize = playableShapeController.getShapeSize();

            if (newPos.y < 0 || newPos.x < 0 || newPos.x + newSize.x > boardSize.x || boardController.doesCollide(playableShapeController, Vector3.zero)) {
                playableShapeController.rotateAroundZ();
                playableShapeController.rotateAroundZ();
                playableShapeController.rotateAroundZ();
            } else {
                hintShapeController.rotateAroundZ();
                updateShapeHint();
            }
        }
    }

    private void checkMovementInput(ShapeController playableShapeController, Vector3 playableShapePos, Vector3Int playableShapeSize, Vector3Int boardSize) {
        float currentCameraAngle = mainCameraController.getCurrentAngle();

        if (Input.GetKeyDown(getCurrentKeyFor(MovementType.RIGHT, currentCameraAngle)) && playableShapePos.x + playableShapeSize.x < boardSize.x) {
            playableShapeController.moveBy(Vector3.right);
            if (boardController.doesCollide(playableShapeController, Vector3.left)) {
                playableShapeController.moveBy(Vector3.left);
            } else {
                updateShapeHint();
            }
        } else if (Input.GetKeyDown(getCurrentKeyFor(MovementType.LEFT, currentCameraAngle)) && playableShapePos.x > 0) {
            playableShapeController.moveBy(Vector3.left);
            if (boardController.doesCollide(playableShapeController, Vector3.right)) {
                playableShapeController.moveBy(Vector3.right);
            } else {
                updateShapeHint();
            }

        }

        if (Input.GetKeyDown(getCurrentKeyFor(MovementType.FORWARD, currentCameraAngle)) && playableShapePos.z + playableShapeSize.z < boardSize.z) {
            playableShapeController.moveBy(Vector3.forward);
            if (boardController.doesCollide(playableShapeController, Vector3.back)) {
                playableShapeController.moveBy(Vector3.back);
            } else {
                updateShapeHint();
            }
        } else if (Input.GetKeyDown(getCurrentKeyFor(MovementType.BACKWARD, currentCameraAngle)) && playableShapePos.z > 0) {
            playableShapeController.moveBy(Vector3.back);
            if (boardController.doesCollide(playableShapeController, Vector3.forward)) {
                playableShapeController.moveBy(Vector3.forward);
            } else {
                updateShapeHint();
            }
        }

        if (Input.GetKey(KeyCode.Space)) {
            playableShapeController.moveBy(new Vector3(0, -0.1f, 0));
        }
    }
}
