﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModel
{
    public enum State {
        PRE_GAME,
        PLAYING,
        LOST
    }

    public float CurrentGameSpeed { get; private set; }
    public int Score { get; set; }
    public State GameState { get; set; }


    public GameModel() {
        CurrentGameSpeed = GameConfig.Instance.InitialGameSpeed;
        Score = 0;
        GameState = State.PLAYING;
    }

    public void update() {
        CurrentGameSpeed += GameConfig.Instance.GameAcceleration * Time.deltaTime;
    }

}
