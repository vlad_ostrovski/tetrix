﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputUtils
{
    public enum MovementType
    {
        RIGHT, LEFT, FORWARD, BACKWARD, ROTATEX, ROTATEZ
    }

    /// <summary>
    /// Returns the correct movement key for a movement type given the current camera angle
    /// </summary>
    public static KeyCode getCurrentKeyFor(MovementType type, float angle) {
        angle *= (180 / Mathf.PI);

        switch (type) {
            case MovementType.RIGHT:
                if (angle >= 225 && angle < 315) {
                    return KeyCode.D;
                } else if (angle >= 315 && angle < 360 || angle >= 0 && angle < 45) {
                    return KeyCode.S;
                } else if (angle >= 45 && angle < 135) {
                    return (KeyCode.A);
                } else {
                    return KeyCode.W;
                }
            case MovementType.LEFT:
                if (angle >= 225 && angle < 315) {
                    return KeyCode.A;
                } else if (angle >= 315 && angle < 360 || angle >= 0 && angle < 45) {
                    return KeyCode.W;
                } else if (angle >= 45 && angle < 135) {
                    return (KeyCode.D);
                } else {
                    return KeyCode.S;
                }
            case MovementType.FORWARD:
                if (angle >= 225 && angle < 315) {
                    return KeyCode.W;
                } else if (angle >= 315 && angle < 360 || angle >= 0 && angle < 45) {
                    return KeyCode.D;
                } else if (angle >= 45 && angle < 135) {
                    return (KeyCode.S);
                } else {
                    return KeyCode.A;
                }
            case MovementType.BACKWARD:
                if (angle >= 225 && angle < 315) {
                    return KeyCode.S;
                } else if (angle >= 315 && angle < 360 || angle >= 0 && angle < 45) {
                    return KeyCode.A;
                } else if (angle >= 45 && angle < 135) {
                    return (KeyCode.W);
                } else {
                    return KeyCode.D;
                }
            case MovementType.ROTATEX:
                if (angle >= 225 && angle < 315) {
                    return KeyCode.Q;
                } else if (angle >= 315 && angle < 360 || angle >= 0 && angle < 45) {
                    return KeyCode.R;
                } else if (angle >= 45 && angle < 135) {
                    return (KeyCode.Q);
                } else {
                    return KeyCode.R;
                }
            case MovementType.ROTATEZ:
                if (angle >= 225 && angle < 315) {
                    return KeyCode.R;
                } else if (angle >= 315 && angle < 360 || angle >= 0 && angle < 45) {
                    return KeyCode.Q;
                } else if (angle >= 45 && angle < 135) {
                    return (KeyCode.R);
                } else {
                    return KeyCode.Q;
                }
            default:
                return KeyCode.None;
        }
    }


}
