﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeModel {

    public int Width { get; private set; }
    public int Height { get; private set; }
    public int Depth { get; private set; }
    public int PivotIdx { get; private set; }
    public Color Color { get; private set; }
    public Vector3 Position { get; set; }
       
    private BitArray shape;

    private int originalWidth;
    private int originalHeight;
    private int originalDepth;

    private Matrix4x4 idxTransform;
    private Matrix4x4 idxTransformInverse;

    private Vector3 currentPivot;
    
    private ShapeModel() {
        idxTransform = Matrix4x4.identity;
        idxTransformInverse = Matrix4x4.identity;
    }

    /// <summary>
    /// Init a shape model using given params
    /// </summary>
    public static ShapeModel initFromString(int width, int height, int depth, Color color, string desc, int pivotIdx) {
        ShapeModel model = new ShapeModel();

        model.Width = width;
        model.originalWidth = width;
        model.Height = height;
        model.originalHeight = height;
        model.Depth = depth;
        model.originalDepth = depth;
        model.Color = color;
        model.PivotIdx = pivotIdx;

        model.shape = new BitArray(width * height * depth);

        string[] entries = desc.Split(',');

        int pivotCounter = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < depth; j++) {
                for (int k = 0; k < width; k++) {
                    string entryStr = entries[i * width * depth + j * width + k];
                    
                    if (entryStr.Trim().Equals("1")) {
                        model.shape.Set(model.getBitIdx(k, i, j), true);

                        if (pivotCounter < pivotIdx) {
                            pivotCounter++;
                            if (pivotCounter == pivotIdx) {
                                model.currentPivot = new Vector3(k, i, j);
                            }
                        }
                    }
                }
            }
        }

        return model;
    }

    /// <summary>
    /// Init a shape model using an existing shape model
    /// </summary>
    public static ShapeModel initFromShapeModel(ShapeModel other) {
        ShapeModel model = new ShapeModel();

        model.Width = other.Width;
        model.originalWidth = other.originalWidth;
        model.Height = other.Height;
        model.originalHeight = other.originalHeight;
        model.Depth = other.Depth;
        model.originalDepth = other.originalDepth;
        model.Color = other.Color;
        model.PivotIdx = other.PivotIdx;
        model.currentPivot = other.currentPivot;

        model.shape = (BitArray)other.shape.Clone();

        model.Position = Vector3.zero;

        return model;
    }

    /// <summary>
    /// Get the edge blocks positions along a certain axis in board coordinates
    /// Note: The axis should be the <b> opposite </b> of the shape's movement direction.
    /// </summary>
    public List<Vector3> getContactPoints(Vector3 axis) {
        int x = 0, y = 0, z = 0;

        if (axis.Equals(Vector3.up)) {
            return traverse(0, Width, 0, Depth, 0, Height, ref x, ref y, ref z, ref x, ref z, ref y);
        } else if (axis.Equals(Vector3.down)) {
            return traverse(0, Width, 0, Depth, Height - 1, 0, ref x, ref y, ref z, ref x, ref z, ref y);
        } else if (axis.Equals(Vector3.right)) {
            return traverse(0, Depth, 0, Height, 0, Width, ref x, ref y, ref z, ref z, ref y, ref x);
        } else if (axis.Equals(Vector3.left)) {
            return traverse(0, Depth, 0, Height, Width - 1, 0, ref x, ref y, ref z, ref z, ref y, ref x);
        } else if (axis.Equals(Vector3.forward)) {
            return traverse(0, Width, 0, Height, 0, Depth, ref x, ref y, ref z, ref x, ref y, ref z);
        } else if (axis.Equals(Vector3.back)) {
            return traverse(0, Width, 0, Height, Depth - 1, 0, ref x, ref y, ref z, ref x, ref y, ref z);
        } else {
            throw new ArgumentException("Only standard axis are supported");
        }

    }

    // This function is just 3 for loops, one for each axis, with configurable order.
    // When the inner loop finds a block, it stops moving along the given axis and moves on.
    private List<Vector3> traverse(int outStart, int outEnd, int middleStart, int middleEnd, int innerStart, int innerEnd, 
                                    ref int x, ref int y, ref int z, ref int outer, ref int mid, ref int inn) {
        List<Vector3> contactPoints = new List<Vector3>();

        for (outer = outStart; (outStart < outEnd && outer < outEnd) || (outStart >= outEnd && outer >= outEnd); outer += (outEnd - outStart == 0 ? -1 : (int)Mathf.Sign(outEnd - outStart))) {
            for (mid = middleStart; (middleStart < middleEnd && mid < middleEnd) || (middleStart >= middleEnd && mid >= middleEnd); mid += (middleEnd - middleStart == 0 ? -1 : (int)Mathf.Sign(middleEnd - middleStart))) {
                for (inn = innerStart; (innerStart < innerEnd && inn < innerEnd) || (innerStart >= innerEnd && inn >= innerEnd); inn += (innerEnd - innerStart == 0 ? -1 : (int)Mathf.Sign(innerEnd - innerStart))) {
                    if (shape.Get(getBitIdx(x, y, z))) {
                        contactPoints.Add(new Vector3(x, y, z));
                        break;
                    }
                }
            }
        }

        return contactPoints;
    }

    /// <summary>
    /// Get shape's block positions in borad coordinates relative to the shape position
    /// </summary>
    public List<Vector3> getShapeBlockPositions() {
        List<Vector3> blockPositions = new List<Vector3>();

        for (int y = 0; y < Height; y++) {
            for (int z = 0; z < Depth; z++) {
                for (int x = 0; x < Width; x++) {
                    if (shape.Get(getBitIdx(x, y, z))) {
                        blockPositions.Add(new Vector3(x, y, z));
                    }
                }
            }
        }

        return blockPositions;
    }

    // this function maps a given coordinate to the correct bit using the updated transform matrix
    private int getBitIdx(int x, int y, int z) {
        Vector3 requested = new Vector3(x, y, z);
        Vector3 transformed = idxTransformInverse.MultiplyPoint3x4(requested);

        int res = Mathf.RoundToInt(transformed.y) * originalWidth * originalDepth + Mathf.RoundToInt(transformed.z) * originalWidth + Mathf.RoundToInt(transformed.x);
        return res;
    }

    /// <summary>
    /// Rotates the shape model around the x axis, around it's pivot 
    /// </summary>
    public void rotateAroundX() {
        Matrix4x4 step = Matrix4x4.identity;

        Vector3 originFix = new Vector3(0, (Depth - currentPivot.z - 1) - currentPivot.y, currentPivot.y - currentPivot.z);
        step *= Matrix4x4.Translate(originFix);
        step *= Matrix4x4.Translate(currentPivot);
        step *= Matrix4x4.Rotate(Quaternion.AngleAxis(90, Vector3.right));
        step *= Matrix4x4.Translate(-currentPivot);

        idxTransform = step * idxTransform;
        idxTransformInverse = idxTransform.inverse;

        currentPivot = step.MultiplyPoint3x4(currentPivot);

        int temp = Height;
        Height = Depth;
        Depth = temp;

        Position -= originFix;
    }

    /// <summary>
    /// Rotates the shape model around the y axis, around it's pivot 
    /// </summary>
    public void rotateAroundY() {
        Matrix4x4 step = Matrix4x4.identity;

        Vector3 originFix = new Vector3((Depth - currentPivot.z - 1) - currentPivot.x, 0, currentPivot.x - currentPivot.z);
        step *= Matrix4x4.Translate(originFix);
        step *= Matrix4x4.Translate(currentPivot);
        step *= Matrix4x4.Rotate(Quaternion.AngleAxis(-90, Vector3.up));
        step *= Matrix4x4.Translate(-currentPivot);
        
        idxTransform = step*idxTransform;
        idxTransformInverse = idxTransform.inverse;

        currentPivot = step.MultiplyPoint3x4(currentPivot);

        int temp = Width;
        Width = Depth;
        Depth = temp;

        Position -= originFix;
    }

    /// <summary>
    /// Rotates the shape model around the z axis, around it's pivot 
    /// </summary>
    public void rotateAroundZ() {
        Matrix4x4 step = Matrix4x4.identity;

        Vector3 originFix = new Vector3((Height - currentPivot.y - 1) - currentPivot.x, currentPivot.x - currentPivot.y, 0);
        step *= Matrix4x4.Translate(originFix);
        step *= Matrix4x4.Translate(currentPivot);
        step *= Matrix4x4.Rotate(Quaternion.AngleAxis(90, Vector3.forward));
        step *= Matrix4x4.Translate(-currentPivot);

        idxTransform = step * idxTransform;
        idxTransformInverse = idxTransform.inverse;

        currentPivot = step.MultiplyPoint3x4(currentPivot);

        int temp = Width;
        Width = Height;
        Height = temp;

        Position -= originFix;
    }

}

