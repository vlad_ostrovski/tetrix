﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ShapeView
{
    private readonly GameObject shape;

    private GameObject[] blocks;
    private GameObject pivotBlock;

    public ShapeView(GameObject shape) {
        this.shape = shape;
    }

    /// <summary>
    /// Init this shape view with the given params
    /// </summary>

    public void initView(List<Vector3> blockPositions, int pivotIdx, Color color) {

        shape.transform.rotation = Quaternion.identity;
        shape.transform.position = Vector3.zero;

        blocks = new GameObject[blockPositions.Count];

        for (int i = 0; i < blocks.Length; i++) {
            GameObject newBlock = ShapeBlockUtils.getBlock(shape.transform);
            newBlock.GetComponent<ShapeBlockController>().placeAt(blockPositions[i]);
            newBlock.GetComponent<ShapeBlockController>().setColor(color);

            if (i == pivotIdx - 1) {
                pivotBlock = newBlock;
            }

            blocks[i] = newBlock;
        }
    }

    /// <summary>
    /// Set all this shape's blocks material to the Hint Block material
    /// </summary>

    public void setHintShape() {
        foreach (GameObject block in blocks) {
            block.GetComponent<ShapeBlockController>().setHintBlock();
        }
    }

    /// <summary>
    /// Moves the shape view by delta in board coordinates
    /// </summary>
    public void moveBy(Vector3 delta) {
        shape.transform.position += delta;
    }

    /// <summary>
    /// Rotates the shape view around the x axis, around it's pivot block
    /// </summary>
    public void rotateAroundX() {
        shape.transform.RotateAround(pivotBlock.transform.position, Vector3.right, 90);
    }

    /// <summary>
    /// Rotates the shape view around the y axis, around it's pivot block
    /// </summary>
    public void rotateAroundY() {
        shape.transform.RotateAround(pivotBlock.transform.position, Vector3.up, -90);
    }

    /// <summary>
    /// Rotates the shape view around the z axis, around it's pivot block
    /// </summary>
    public void rotateAroundZ() {
        shape.transform.RotateAround(pivotBlock.transform.position, Vector3.forward, 90);
    }

    /// <summary>
    /// Rotates the shape view around a world position by degrees
    /// </summary>

    public void rotateAroundWorldPoint(Vector3 worldPosition, Vector3 up, float degrees) {
        shape.transform.RotateAround(worldPosition, up, degrees);
    }

    /// <summary>
    /// Destroys all blocks views of this shape view
    /// </summary>
    public void destroy() {
        if (blocks == null) {
            return;
        }

        foreach (GameObject block in blocks) {
            ShapeBlockUtils.destroyBlock(block);
        }

        blocks = null;
        pivotBlock = null;
    }
}
