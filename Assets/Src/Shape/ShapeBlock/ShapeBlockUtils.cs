﻿using UnityEngine;

public class ShapeBlockUtils
{
    /// <summary>
    /// Instantiate a block game object and attach it to given parent
    /// </summary>
    public static GameObject getBlock(Transform parent) {
        return Object.Instantiate(Object.FindObjectOfType<Prefebs>().BlockPrefeb, Vector3.zero, Quaternion.identity, parent);
    }

    /// <summary>
    /// Destroy a block game object
    /// </summary>
    /// <param name="block"></param>
    public static void destroyBlock(GameObject block) {
        UnityEngine.Object.Destroy(block);
    }
}
