﻿using UnityEngine;

public class ShapeBlockController : MonoBehaviour
{
    public Material HintMaterial;

    private readonly float FadeTime = 0.2f;
    private readonly float BounceTime = 0.35f;

    private bool isAnimating;
    private float timer;
    private Vector3 startingPosition;
    private Vector3 endPosition;
    private Vector3 velocity;
    private bool fading;

    void Start() {
        GetComponent<MeshRenderer>().material.SetFloat("_BoardHeight", GameConfig.Instance.BoardHeight);
    }

    void Update() {
        if (isAnimating) {
            if (fading) {
                timer += Time.deltaTime;
                this.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, timer / FadeTime);
                this.transform.position += velocity * Time.deltaTime;

                velocity.y += 40 * Time.deltaTime;


                if (timer >= FadeTime) {
                    isAnimating = false;
                    fading = false;
                    ShapeBlockUtils.destroyBlock(this.gameObject);
                }
            } else {
                timer += Time.deltaTime;
                this.transform.position = Vector3.Lerp(startingPosition, endPosition, getInterpolation(timer / BounceTime));

                if (timer >= BounceTime) {
                    isAnimating = false;
                }
            }
        }
    }


    /////////////////////////////////////////////////////////////////////////
    // This is taken from the Android Bounce Interpolator because i like it

    private float bounce(float t) {
        return t * t * 8.0f;
    }

    private float getInterpolation(float t) {

        t *= 1.1226f;
        if (t < 0.3535f) return bounce(t);
        else if (t < 0.7408f) return bounce(t - 0.54719f) + 0.7f;
        else if (t < 0.9644f) return bounce(t - 0.8526f) + 0.9f;
        else return bounce(t - 1.0435f) + 0.95f;
    }

    /////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Set's the block view color
    /// </summary>
    public void setColor(Color color) {
        GetComponent<MeshRenderer>().material.SetColor("_BaseColor", color);
    }

    /// <summary>
    /// Changes the block view's material to the Hint Block material
    /// </summary>

    public void setHintBlock() {
        GetComponent<MeshRenderer>().material = HintMaterial;
    }

    /// <summary>
    /// Places the block view at the correct position given a board position
    /// </summary>
    public void placeAt(Vector3 pos) {
        this.transform.localPosition = pos + this.transform.localScale / 2;
    }

    /// <summary>
    /// Starts the block view's destruction animation <br/>
    /// Note: When this animation finishes, the block self destructs
    /// </summary>

    public void fadeOut() {
        timer = 0;
        startingPosition = this.transform.position;
        velocity = 20*(startingPosition - new Vector3(GameConfig.Instance.BoardWidth / 2f, startingPosition.y + 3*UnityEngine.Random.value, GameConfig.Instance.BoardDepth / 2f));
        isAnimating = true;
        fading = true;
    }

    /// <summary>
    /// Starts the block view's move and bounce animation to the given a board y coordinate
    /// </summary>
    public void bringToPlane(int y) {
        startingPosition = this.transform.position;

        startingPosition.y += 0.25f;
        endPosition = new Vector3(startingPosition.x, y + 0.5f, startingPosition.z);

        timer = 0;
        isAnimating = true;
        fading = false;
    }

}
