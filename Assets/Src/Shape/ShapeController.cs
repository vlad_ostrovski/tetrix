﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ShapeController : MonoBehaviour
{
    private ShapeModel shapeModel;
    private ShapeView shapeView;

    /// <summary>
    /// Init this shape from a new random model
    /// </summary>
    public void initNextShape() {
        initNextShape(AvailableShapes.Instance.getRandomShape());

    }

    /// <summary>
    /// Init this shape from another shape's model
    /// </summary>
    public void initNextShape(ShapeController other) {
        initNextShape(other.shapeModel);
    }

    private void initNextShape(ShapeModel shape) {
        shapeModel = ShapeModel.initFromShapeModel(shape);
        shapeView = new ShapeView(this.gameObject);

        List<Vector3> blockPositions = shapeModel.getShapeBlockPositions();
        shapeView.initView(blockPositions, shapeModel.PivotIdx, shapeModel.Color);
    }

    /// <summary>
    /// Set this shape as the hint shape
    /// </summary>
    public void setHintShape() {
        shapeView.setHintShape();
    }

    /// <summary>
    /// Get shape's block positions in borad coordinates relative to the shape position
    /// </summary>
    public List<Vector3> getLocalShapeBlockPositions() {
        return shapeModel.getShapeBlockPositions(); 
    }

    /// <summary>
    /// Get the edge blocks positions along a certain axis in board coordinates
    /// Note: The axis should be the <b> opposite </b> of the shape's movement direction.
    /// </summary>
    public List<Vector3> getLocalContactPoints(Vector3 axis) {
        return shapeModel.getContactPoints(axis);
    }

    /// <summary>
    /// Moves the shape down given a certain speed
    /// </summary>
    public void drop(float speed) {
        shapeModel.Position -= new Vector3(0, speed * Time.deltaTime, 0);
        shapeView.moveBy(new Vector3(0, -speed * Time.deltaTime, 0));
    }

    /// <summary>
    /// Returns the shape's size
    /// </summary>
    public Vector3Int getShapeSize() {
        return new Vector3Int(shapeModel.Width, shapeModel.Height, shapeModel.Depth);
    }

    /// <summary>
    /// Moves the shape by delta in board coordinates
    /// </summary>
    public void moveBy(Vector3 delta) {
        shapeModel.Position += delta;
        shapeView.moveBy(delta);
    }

    /// <summary>
    /// Move the shape to given position in board coordinates
    /// </summary>
    public void moveTo(Vector3 pos) {
        var diff = pos - shapeModel.Position;
        moveBy(diff);
    }

    /// <summary>
    /// Returns the shape current position in board coordinates
    /// </summary>
    public Vector3 GetShapePosition() {
        return shapeModel.Position;
    }

    /// <summary>
    /// Rotate the shape around the x axis, around it's pivot point
    /// </summary>
    public void rotateAroundX() {
        shapeModel.rotateAroundX();
        shapeView.rotateAroundX();
    }

    /// <summary>
    /// Rotate the shape around the y axis, around it's pivot point
    /// </summary>
    public void rotateAroundY() {
        shapeModel.rotateAroundY();
        shapeView.rotateAroundY();
    }

    /// <summary>
    /// Rotate the shape around the z axis, around it's pivot point
    /// </summary>
    public void rotateAroundZ() {
        shapeModel.rotateAroundZ();
        shapeView.rotateAroundZ();
    }

    /// <summary>
    /// Rotate the shape view around it's center
    /// </summary>

    public void rotatePreview() {
        shapeView.rotateAroundWorldPoint(shapeModel.Position + new Vector3(shapeModel.Width / 2f, shapeModel.Height / 2f, shapeModel.Depth / 2f), Vector3.up, -30 * Time.deltaTime);
    }

    /// <summary>
    /// Returns the shape color
    /// </summary>

    public Color getShapeColor() {
        return shapeModel.Color;
    }

    /// <summary>
    /// Destroys the shape and the shape's view
    /// </summary>
    public void destroy() {
        shapeModel = null;

        if (shapeView != null) {
            shapeView.destroy();
        }
    }
}
